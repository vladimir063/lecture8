package org.sber;

import org.sber.exception.TransferAmountValidationException;
import org.sber.exception.TranslationSecurityCheckException;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class FrodMonitor {

    List<LocalTime> listTime = new ArrayList();

    public void translationSecurityCheck(){
        listTime.add(LocalTime.now());
        validateListTime();
    }

    private void validateListTime(){
        if (listTime.size() > 15 && listTime.get(0).getHour() == listTime.get(15).getHour()) {
            throw new TranslationSecurityCheckException("Ваш аккаунт заблокирован в связи с подозрительными переводами в течении последнего часа. " +
                    "Пожалуйста свяжитесь с оператором");
        }
    }

    public void transferAmountValidation(int total) {
        if (total % 100 != 0) {
            throw new TransferAmountValidationException("Ошибка при выполнении перевода. Сумма перевода быть кратна 100. Повторите перевод");
        }
    }
}
