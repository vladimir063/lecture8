package org.sber.exception;

public class PinNotEnteredException extends RuntimeException{

    public PinNotEnteredException(String message) {
        super(message);
    }
}
