package org.sber.exception;

public class TranslationSecurityCheckException extends RuntimeException{

    public TranslationSecurityCheckException(String message) {
        super(message);
    }
}
