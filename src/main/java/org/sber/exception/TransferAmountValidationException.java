package org.sber.exception;

public class TransferAmountValidationException  extends RuntimeException{

    public TransferAmountValidationException(String message) {
        super(message);
    }
}
