package org.sber;

import lombok.Data;
import org.sber.exception.PinNotEnteredException;
import org.sber.exception.PinValidatorException;
import org.sber.exception.TransferAmountValidationException;


@Data
public class TerminalImpl implements Terminal {

    private final TerminalServer server;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;



    public void setPinCode(String pinCode) {
        pinValidator.setPinCode(pinCode);
    }

    public boolean transferFromAccountToAccount(String sender, String receiver, int total) {
        if (pinValidator.isCorrect()) {
            frodMonitor.transferAmountValidation(total);
            frodMonitor.translationSecurityCheck();
            server.doTransferAccountToAccount(sender, receiver, total);
            return true;
        } else {
            throw new PinValidatorException("Некорректный пин код. Попробуйте снова");
        }
    }



}
