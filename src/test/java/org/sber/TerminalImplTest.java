package org.sber;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.sber.exception.PinValidatorException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class TerminalImplTest {

    @Mock
    PinValidator pinValidator;

    @Mock
    FrodMonitor frodMonitor;

    @Mock
    TerminalServer terminalServer;

    @InjectMocks
    TerminalImpl terminal;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        terminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
    }

    @Test
    public void testSetPinCode() {
        terminal.setPinCode(anyString());

        verify(pinValidator, times(1)).setPinCode(anyString());
    }


    @Test
    public void testTransferFromAccountToAccountUnsuccess() {
        Mockito.when(pinValidator.isCorrect()).thenReturn(false);

        assertThrows(PinValidatorException.class, () -> {
            terminal.transferFromAccountToAccount("5455646", "54512", 100);
        });
    }

    @Test
    public void testTransferFromAccountToAccountSuccess() {
        Mockito.when(pinValidator.isCorrect()).thenReturn(true);

        boolean actual = terminal.transferFromAccountToAccount("5455646", "54512", 100);

        assertTrue(actual);
    }

}
