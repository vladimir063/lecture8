package org.sber;

import org.sber.exception.AccountIsLockedException;
import org.sber.exception.PinValidatorException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PinValidatorTest {

    PinValidator pinValidator;

    @BeforeMethod
    public void setUp() {
        pinValidator = new PinValidator("0000");
    }

    @Test
    public void testSetPinCodeCount() {
        pinValidator.setPinCode("5555");

        assertEquals(pinValidator.getCount(), 1);
    }

    @Test
    public void testSetPinCodeSuccess() {
        pinValidator.setPinCode("0000");
    }

    @Test(expectedExceptions = AccountIsLockedException.class)
    public void testSetPinCode2() {
        pinValidator.setPinCode("5555");
        pinValidator.setPinCode("5555");
        pinValidator.setPinCode("5555");
        pinValidator.setPinCode("5555");
    }


}
